from threading import Thread, Event
import time

movement_detected = Event()
end_program = Event()

class TrafficLight:
    def __init__(self, delay=0.001):
        self.time_to_sleep = delay
    
    def runSimulation(self, movement_detected):
        while True:
            time.sleep(self.time_to_sleep)
            if end_program.is_set():
                return
            if movement_detected.is_set():
                print("Movement detected, needs to check if Traffic light is Red, then it needs to take picture/video")
            else:
                print("Simulation is running... Traffic in green, simulation continue...")

class MovementSensor:
    def __init__(self, delay=0.5):
        self.time_to_sleep = delay
    
    def detectMovement(self, movement_detected):
        while True:
            time.sleep(self.time_to_sleep)
            if end_program.is_set():
                return
            if movement_detected.is_set():
                movement_detected.clear()
            else:
                movement_detected.set()



def main():
    end_program.clear()

    trafficLight = TrafficLight(1)
    trafficThread = Thread(target=trafficLight.runSimulation, args=(movement_detected, ))
    
    movementSensor = MovementSensor(2)
    movementSensorThread = Thread(target=movementSensor.detectMovement, args=(movement_detected, ))
    
    movementSensorThread.start()
    trafficThread.start()

    movementSensorThread.join()
    trafficThread.join()

def destroy():
     print("Remember to also clear the GPIO state...")
     end_program.set()
     print("Finishing Traffic Light simulation...")

if __name__ == '__main__':     # Program entrance
    print ('Program is starting ... ')
    try:
        main()
    except KeyboardInterrupt:  # Press ctrl-c to end the program.
        destroy()