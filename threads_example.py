from threading import Thread, Lock
import time

def camera_function(message):
	print('Camera starts taking a picture' + " and " + message)
	time.sleep(2)
	print('Camera ends taking a picture')

movement_detected = True;
lock = Lock()
if movement_detected :
    # Start a new thread to take a picture
    thread = Thread(target=camera_function, args=('passing a string to a thread',))
    thread.start()
    thread.join()
    # The program is ready to continue after join
    with lock:
         movement_detected = False;
    print('Camera is not being used')
